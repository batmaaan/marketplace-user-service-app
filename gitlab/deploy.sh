mvn clean compile deploy
mvn compile -pl marketplace-user-service -U com.google.cloud.tools:jib-maven-plugin:3.2.1:build -Djib.to.auth.username=$DOCKER_USERNAME -Djib.to.auth.password=$DOCKER_PASS  -Dimage=batmaaan/user-service:1.0-SNAPSHOT
cd /home/finch/app/user; docker-compose pull; docker-compose stop; docker-compose up -d;
