package org.marketplace.user.client;

import org.marketplace.user.client.operation.auth.UserAuthOperation;
import org.marketplace.user.client.operation.profile.UserProfileOperation;
import org.marketplace.user.client.operation.registration.UserRegistrationOperation;
import org.marketplace.user.client.operation.user.UserOperation;

public interface UserServiceClient {

  UserProfileOperation profile();

  UserAuthOperation auth();

  UserRegistrationOperation registration();

  UserOperation user();

}
