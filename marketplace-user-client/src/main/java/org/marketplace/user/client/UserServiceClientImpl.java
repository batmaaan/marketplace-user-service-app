package org.marketplace.user.client;

import org.marketplace.user.client.operation.auth.UserAuthOperation;
import org.marketplace.user.client.operation.auth.UserAuthOperationImpl;
import org.marketplace.user.client.operation.profile.UserProfileOperation;
import org.marketplace.user.client.operation.profile.UserProfileOperationImpl;
import org.marketplace.user.client.operation.registration.UserRegistrationOperation;
import org.marketplace.user.client.operation.registration.UserRegistrationOperationImpl;
import org.marketplace.user.client.operation.user.UserOperation;
import org.marketplace.user.client.operation.user.UserOperationImpl;

import java.net.http.HttpClient;
import java.time.Duration;

public class UserServiceClientImpl implements UserServiceClient {
  private final UserProfileOperation profileOperation;

  private final UserAuthOperation authOperation;

  private final UserRegistrationOperation registrationOperation;
  private final UserOperation userOperation;

  public UserServiceClientImpl(String url) {

    HttpClient client = HttpClient.newBuilder()
      .version(HttpClient.Version.HTTP_1_1)
      .followRedirects(HttpClient.Redirect.NORMAL)
      .connectTimeout(Duration.ofSeconds(5))
      .build();

    this.profileOperation = new UserProfileOperationImpl(client, url);
    this.authOperation = new UserAuthOperationImpl(client, url);
    this.registrationOperation = new UserRegistrationOperationImpl(client, url);
    this.userOperation = new UserOperationImpl(client, url);
  }

  @Override
  public UserProfileOperation profile() {
    return profileOperation;
  }

  @Override
  public UserAuthOperation auth() {
    return authOperation;
  }

  @Override
  public UserRegistrationOperation registration() {
    return registrationOperation;
  }

  @Override
  public UserOperation user() {
    return userOperation;
  }

}
