package org.marketplace.user.client.operation.auth;

import org.marketplace.user.model.request.UserLoginRequest;
import org.marketplace.user.model.response.InformationResponse;

public interface UserAuthOperation {
  InformationResponse login(UserLoginRequest loginRequest);
}
