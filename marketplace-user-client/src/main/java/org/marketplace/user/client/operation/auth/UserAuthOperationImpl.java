package org.marketplace.user.client.operation.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.marketplace.user.model.request.UserLoginRequest;
import org.marketplace.user.model.response.InformationResponse;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

import static org.marketplace.user.model.utils.UrlEndpoints.AUTH_URI;

public class UserAuthOperationImpl implements UserAuthOperation {

  ObjectMapper objectMapper = new ObjectMapper();
  private final HttpClient httpClient;
  private final String URL;


  public UserAuthOperationImpl(HttpClient client, String url) {
    this.httpClient = client;
    this.URL = url;
  }

  @SneakyThrows
  @Override
  public InformationResponse login(UserLoginRequest loginRequest) {
    String json = objectMapper.writeValueAsString(loginRequest);
    HttpRequest request = HttpRequest.newBuilder()
      .uri(new URI( URL + AUTH_URI))
      .timeout(Duration.ofSeconds(5))
      .header("Content-Type", "application/json")
      .POST(HttpRequest.BodyPublishers.ofString(json))
      .build();

    var responce = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    return objectMapper.readValue(responce.body(), InformationResponse.class);
  }
}
