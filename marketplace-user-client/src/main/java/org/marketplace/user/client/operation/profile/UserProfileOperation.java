package org.marketplace.user.client.operation.profile;

import org.marketplace.user.model.request.UpdateProfileRequest;
import org.marketplace.user.model.response.InformationProfileResponse;

public interface UserProfileOperation {

  InformationProfileResponse getProfile(Integer userId);

  InformationProfileResponse updateProfile(UpdateProfileRequest profile);


}
