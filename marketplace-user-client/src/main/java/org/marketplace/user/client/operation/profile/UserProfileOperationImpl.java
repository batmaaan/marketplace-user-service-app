package org.marketplace.user.client.operation.profile;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.marketplace.user.model.request.UpdateProfileRequest;
import org.marketplace.user.model.response.InformationProfileResponse;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

import static org.marketplace.user.model.utils.UrlEndpoints.GET_PROFILE_URI;
import static org.marketplace.user.model.utils.UrlEndpoints.UPDATE_PROFILE_URI;


public class UserProfileOperationImpl implements UserProfileOperation {
  ObjectMapper objectMapper = new ObjectMapper();
  private final HttpClient httpClient;
  private final String URL;


  public UserProfileOperationImpl(HttpClient client, String url) {
    this.httpClient = client;
    this.URL = url;
  }

  @SneakyThrows
  @Override
  public InformationProfileResponse getProfile(Integer userId) {
    HttpRequest request = HttpRequest.newBuilder()
      .uri(new URI(URL + GET_PROFILE_URI + "?userId=" + userId))
      .GET()
      .build();

    var responce = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    return objectMapper.readValue(responce.body(), InformationProfileResponse.class);
  }

  @SneakyThrows
  @Override
  public InformationProfileResponse updateProfile(UpdateProfileRequest profile) {
    String json = objectMapper.writeValueAsString(profile);
    HttpRequest request = HttpRequest.newBuilder()
      .uri(new URI(URL +UPDATE_PROFILE_URI))
      .timeout(Duration.ofSeconds(5))
      .header("Content-Type", "application/json")
      .POST(HttpRequest.BodyPublishers.ofString(json))
      .build();

    var responce = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    return objectMapper.readValue(responce.body(), InformationProfileResponse.class);
  }
}
