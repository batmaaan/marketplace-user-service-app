package org.marketplace.user.client.operation.registration;

import org.marketplace.user.model.request.UserRegistrationRequest;
import org.marketplace.user.model.response.RegistrationResponse;

public interface UserRegistrationOperation {

  RegistrationResponse registration(UserRegistrationRequest registrationRequest);
}
