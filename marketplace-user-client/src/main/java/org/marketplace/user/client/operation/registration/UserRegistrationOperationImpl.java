package org.marketplace.user.client.operation.registration;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.marketplace.user.model.request.UserRegistrationRequest;
import org.marketplace.user.model.response.RegistrationResponse;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

import static org.marketplace.user.model.utils.UrlEndpoints.REGISTRATION_URI;


public class UserRegistrationOperationImpl implements UserRegistrationOperation {

  ObjectMapper objectMapper = new ObjectMapper();
  private final HttpClient httpClient;
  private final String URL;

  public UserRegistrationOperationImpl(HttpClient client, String url) {
    this.httpClient = client;
    this.URL = url;
  }

  @SneakyThrows
  @Override
  public RegistrationResponse registration(UserRegistrationRequest registrationRequest) {
    String json = objectMapper.writeValueAsString(registrationRequest);

    HttpRequest request = HttpRequest.newBuilder()
      .uri(new URI(URL + REGISTRATION_URI))
      .timeout(Duration.ofSeconds(5))
      .header("Content-Type", "application/json")
      .POST(HttpRequest.BodyPublishers.ofString(json))
      .build();

    var responce = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    return objectMapper.readValue(responce.body(), RegistrationResponse.class);
  }
}
