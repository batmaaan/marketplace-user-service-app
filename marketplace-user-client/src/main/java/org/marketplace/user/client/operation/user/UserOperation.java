package org.marketplace.user.client.operation.user;

import org.marketplace.user.model.User;

public interface UserOperation {
  User getUserByEmail(String mail);
}
