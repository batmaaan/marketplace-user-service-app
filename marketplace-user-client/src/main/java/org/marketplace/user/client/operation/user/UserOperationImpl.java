package org.marketplace.user.client.operation.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.marketplace.user.model.User;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.marketplace.user.model.utils.UrlEndpoints.USER_BY_EMAIL_URI;

public class UserOperationImpl implements UserOperation {
  ObjectMapper objectMapper = new ObjectMapper();
  private final HttpClient httpClient;
  private final String URL;


  public UserOperationImpl(HttpClient client, String url) {
    this.httpClient = client;
    this.URL = url;
  }

  @Override
  @SneakyThrows
  public User getUserByEmail(String mail) {
    HttpRequest request = HttpRequest.newBuilder()
      .uri(new URI(URL + USER_BY_EMAIL_URI + "?mail=" + mail))
      .GET()
      .build();

    var responce = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    return objectMapper.readValue(responce.body(), User.class);
  }
}
