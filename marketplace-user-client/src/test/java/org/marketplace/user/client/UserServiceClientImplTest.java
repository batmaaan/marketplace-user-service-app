package org.marketplace.user.client;

import org.junit.jupiter.api.Test;
import org.marketplace.user.client.utils.InitUser;
import org.marketplace.user.model.request.UpdateProfileRequest;
import org.marketplace.user.model.request.UserLoginRequest;
import org.marketplace.user.model.request.UserRegistrationRequest;

import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class UserServiceClientImplTest {
//  private final UserServiceClient serviceClient = new UserServiceClientImpl("http://localhost:8080");
//
//  @Autowired
//  private JwtService jwtService = new JwtService();
//
//  @Test
//  void getProfile() {
//    var randomUser = InitUser.initRandomUser();
//    var registrationRequest = new UserRegistrationRequest(
//      randomUser.getFirstName(),
//      randomUser.getLastName(),
//      randomUser.getEmail(),
//      randomUser.getPassword(),
//      randomUser.getBirthDate());
//
//    var registrationResponce = serviceClient.registration().registration(registrationRequest);
//    var profileResponce = serviceClient.profile().getProfile(jwtService.decode(registrationResponce.getToken()).getId());
//
//    assertEquals("ok", profileResponce.getStatus());
//    assertEquals("", profileResponce.getDescription());
//    assertNotNull(profileResponce);
//  }
//
//  @Test
//  void updateProfile() {
//    var randomUser = InitUser.initRandomUser();
//    var registrationRequest = new UserRegistrationRequest(
//      randomUser.getFirstName(),
//      randomUser.getLastName(),
//      randomUser.getEmail(),
//      randomUser.getPassword(),
//      randomUser.getBirthDate());
//    var randomProfile = InitUser.initRandomProfile();
//
//    var registrationResponce = serviceClient.registration().registration(registrationRequest);
//    var profile = serviceClient.profile().getProfile(jwtService.decode(registrationResponce.getToken()).getId());
//    var profileResponce = serviceClient.profile().updateProfile(
//      new UpdateProfileRequest(
//        profile.getResult().getUserId(),
//        randomProfile.getFirstName(),
//        randomProfile.getLastName()));
//
//    assertEquals("ok", profileResponce.getStatus());
//    assertEquals("profile updated", profileResponce.getDescription());
//    assertEquals(profile.getResult().getUserId(), profileResponce.getResult().getUserId());
//    assertEquals(randomProfile.getFirstName(), profileResponce.getResult().getFirstName());
//    assertEquals(randomProfile.getLastName(), profileResponce.getResult().getLastName());
//  }
//
//  @Test
//  void auth() {
//    var randomUser = InitUser.initRandomUser();
//    var registrationRequest = new UserRegistrationRequest(
//      randomUser.getFirstName(),
//      randomUser.getLastName(),
//      randomUser.getEmail(),
//      randomUser.getPassword(),
//      randomUser.getBirthDate());
//
//    serviceClient.registration().registration(registrationRequest);
//
//    var loginRequest = new UserLoginRequest(randomUser.getEmail(), randomUser.getPassword());
//
//    var responce = serviceClient.auth().login(loginRequest);
//
//    assertEquals("ok", responce.getStatus());
//    assertEquals(randomUser.getFirstName(), responce.getResult().getFirstName());
//    assertEquals(randomUser.getLastName(), responce.getResult().getLastName());
//    assertEquals(randomUser.getEmail(), responce.getResult().getEmail());
//
//  }
//
//  @Test
//  void registration() {
//    var randomUser = InitUser.initRandomUser();
//    var registrationRequest = new UserRegistrationRequest(
//      randomUser.getFirstName(),
//      randomUser.getLastName(),
//      randomUser.getEmail(),
//      randomUser.getPassword(),
//      randomUser.getBirthDate());
//
//    var responce = serviceClient.registration().registration(registrationRequest);
//
//    assertEquals("ok", responce.getStatus());
//
//  }

}
