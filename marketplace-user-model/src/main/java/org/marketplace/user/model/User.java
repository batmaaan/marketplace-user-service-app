package org.marketplace.user.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {

  private Integer id;
  private String firstName;
  private String lastName;
  private String email;
  private String password;
  private Date birthDate;
  private String city;


}
