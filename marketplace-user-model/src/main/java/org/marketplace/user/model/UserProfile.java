package org.marketplace.user.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserProfile {

  private Integer id;
  private Integer userId;
  private String firstName;
  private String lastName;

}
