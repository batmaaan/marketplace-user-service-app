package org.marketplace.user.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class InformationProfileResponse {

  private String status;
  private String description;
  private ProfileResponse result;

  public static InformationProfileResponse info(InformationProfileResponse response) {
    return InformationProfileResponse.builder()
      .status(response.getStatus())
      .description(response.getDescription())
      .result(response.getResult())
      .build();

  }


}
