package org.marketplace.user.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class InformationResponse {

  private String status;
  private String description;
  private UserResponse result;

  public static InformationResponse info(InformationResponse response) {
    return InformationResponse.builder()
      .status(response.getStatus())
      .description(response.getDescription())
      .result(response.getResult())
      .build();

  }


}
