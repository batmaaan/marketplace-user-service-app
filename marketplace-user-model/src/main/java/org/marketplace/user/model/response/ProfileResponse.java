package org.marketplace.user.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.marketplace.user.model.UserProfile;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProfileResponse {
  private Integer userId;
  private String firstName;
  private String lastName;


  public static ProfileResponse fromProfile(UserProfile userProfile) {
    return ProfileResponse.builder()
      .userId(userProfile.getUserId())
      .firstName(userProfile.getFirstName())
      .lastName(userProfile.getLastName())
      .build();
  }
}
