package org.marketplace.user.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.marketplace.user.model.User;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationResponse {

  private String status;
  private String description;
  private User user;

}
