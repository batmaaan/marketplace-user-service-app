package org.marketplace.user.model.utils;

public class UrlEndpoints {

  public static final String AUTH_URI = "/marketplace/user/service/auth/login";
  public static final String REGISTRATION_URI = "/marketplace/user/service/auth/registration";
  public static final String USER_BY_EMAIL_URI = "/marketplace/user_service/by-mail";
  public static final String GET_PROFILE_URI = "/marketplace/user/profile/profile";
  public static final String UPDATE_PROFILE_URI = "/marketplace/user/profile/update";
}
