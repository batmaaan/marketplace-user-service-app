package org.marketplace.user.service.api;


import org.marketplace.user.model.request.UserLoginRequest;
import org.marketplace.user.model.response.InformationResponse;
import org.marketplace.user.service.data.service.AuthService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.marketplace.user.model.utils.UrlEndpoints.AUTH_URI;

@RestController
public class AuthController {

  private final AuthService authService;
  public AuthController(AuthService service) {
    this.authService = service;
  }

  @PostMapping(path = AUTH_URI)
  public InformationResponse login(@RequestBody UserLoginRequest loginRequest) {
    return authService.login(loginRequest);
  }

}
