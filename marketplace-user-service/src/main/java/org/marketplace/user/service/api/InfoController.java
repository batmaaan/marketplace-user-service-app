package org.marketplace.user.service.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class InfoController {

  @GetMapping("/heath/check")
  public Map<String, String> healthCheck() {
    return Map.of(
      "status", "ok"
    );
  }
}
