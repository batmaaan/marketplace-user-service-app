package org.marketplace.user.service.api;


import lombok.AllArgsConstructor;
import org.marketplace.user.model.User;
import org.marketplace.user.model.request.UserRegistrationRequest;
import org.marketplace.user.model.response.RegistrationResponse;
import org.marketplace.user.service.data.service.RegistrationService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.marketplace.user.model.utils.UrlEndpoints.REGISTRATION_URI;

@AllArgsConstructor
@RestController
public class RegistrationController {

  private final RegistrationService registrationService;

  @PostMapping(path = REGISTRATION_URI)
  public RegistrationResponse registration(@RequestBody UserRegistrationRequest registrationRequest) {

    User user = new User();
    user.setFirstName(registrationRequest.getFirstName());
    user.setLastName(registrationRequest.getLastName());
    user.setEmail(registrationRequest.getEmail());
    user.setPassword(registrationRequest.getPassword());
    user.setBirthDate(registrationRequest.getBirthDate());
    return registrationService.registration(registrationRequest);

  }


}
