package org.marketplace.user.service.api;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.marketplace.user.model.User;
import org.marketplace.user.service.data.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.marketplace.user.model.utils.UrlEndpoints.USER_BY_EMAIL_URI;

@Slf4j
@RestController
@AllArgsConstructor
public class UserController {
  private final UserService userService;

  @GetMapping(USER_BY_EMAIL_URI)
  public User getUserByEmail(@RequestParam String mail){
    return userService.getUserByEmail(mail);
  }


}
