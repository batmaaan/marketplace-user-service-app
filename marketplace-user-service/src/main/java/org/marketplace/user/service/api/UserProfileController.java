package org.marketplace.user.service.api;


import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.marketplace.user.model.UserProfile;
import org.marketplace.user.model.request.UpdateProfileRequest;
import org.marketplace.user.model.response.InformationProfileResponse;
import org.marketplace.user.model.response.ProfileResponse;
import org.marketplace.user.service.data.service.UserProfileService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.marketplace.user.model.utils.UrlEndpoints.GET_PROFILE_URI;
import static org.marketplace.user.model.utils.UrlEndpoints.UPDATE_PROFILE_URI;


@Slf4j
@RestController
@AllArgsConstructor
public class UserProfileController {

  private final UserProfileService userProfileService;


  @GetMapping(GET_PROFILE_URI)
  public InformationProfileResponse getProfile(Integer userId) {
    UserProfile profileByUserId = userProfileService.getProfileByUserId(userId);
    return InformationProfileResponse.info(
      new InformationProfileResponse(
        "ok",
        "",
        new ProfileResponse(
          profileByUserId.getUserId(),
          profileByUserId.getFirstName(),
          profileByUserId.getLastName())
      )
    );
  }

  @PostMapping(UPDATE_PROFILE_URI)
  public InformationProfileResponse updateProfile(@RequestBody UpdateProfileRequest profile) {
    if (profile != null) {
      ProfileResponse updatedProfile = userProfileService.update(profile.getUserId(), profile);
      return new InformationProfileResponse(
        "ok",
        "profile updated",
        new ProfileResponse(
          updatedProfile.getUserId(),
          updatedProfile.getFirstName(),
          updatedProfile.getLastName()
        )
      );

    } else {
      log.warn("can`t find user");
    }
    return null;
  }
}
