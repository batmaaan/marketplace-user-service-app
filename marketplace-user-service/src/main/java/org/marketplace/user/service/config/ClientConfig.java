package org.marketplace.user.service.config;

import org.marketplace.mail.client.MailServiceClient;
import org.marketplace.mail.client.MailServiceClientImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ClientConfig {

  @Value("${mail.service.url}")
  private String urlMail;

  @Bean
  public MailServiceClient mailServiceClient() {
    return new MailServiceClientImpl(urlMail);
  }
}
