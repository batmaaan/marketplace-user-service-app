package org.marketplace.user.service.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
public class DBConfig {

  @Bean
  JdbcTemplate jdbcTemplate(DataSource dataSource) {
    return new JdbcTemplate(dataSource);
  }

  @Bean
  public DataSource dataSource(@Value("${app.datasource.url}") String url,
                               @Value("${app.datasource.username}") String username,
                               @Value("${app.datasource.password}") String password) {
    var hikariConfig = new HikariConfig();
    hikariConfig.setUsername(username);
    hikariConfig.setPassword(password);
    hikariConfig.setAutoCommit(true);
    hikariConfig.setMaximumPoolSize(10);
    hikariConfig.setIdleTimeout(3000);
    hikariConfig.setConnectionTimeout(3000);
    hikariConfig.setJdbcUrl(url);
    hikariConfig.addDataSourceProperty("ApplicationName", "marketplace-user-service");
    var props = new Properties();
    props.setProperty("sslMode", "disable");
    hikariConfig.setDataSourceProperties(props);
    return new HikariDataSource(hikariConfig);
  }


}
