package org.marketplace.user.service.data.repository;

import org.marketplace.user.model.UserProfile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserProfileRepository {

  private final JdbcTemplate jdbcTemplate;

  private static final String ID = "id";
  private static final String USER_ID = "userid";
  private static final String FIRST_NAME = "firstname";
  private static final String LAST_NAME = "lastname";

  public UserProfileRepository(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public UserProfile getProfileById(Integer id) {
    return jdbcTemplate.execute((Connection con) -> {
      var statement = con.prepareStatement("select * from user_profile where id = ?");
      statement.setInt(1, id);

      var resultSet = statement.executeQuery();

      var res = new UserProfile();
      while (resultSet.next()) {
        res.setId(resultSet.getInt(ID));
        res.setUserId(resultSet.getInt(USER_ID));
        res.setFirstName(resultSet.getString(FIRST_NAME));
        res.setLastName(resultSet.getString(LAST_NAME));
      }
      return res;
    });
  }

  public UserProfile getProfileByUserId(Integer userId) {
    return jdbcTemplate.execute((Connection con) -> {
      var statement = con.prepareStatement("select * from user_profile where userid = ?");
      statement.setInt(1, userId);

      var resultSet = statement.executeQuery();

      var res = new UserProfile();
      while (resultSet.next()) {
        res.setId(resultSet.getInt(ID));
        res.setUserId(resultSet.getInt(USER_ID));
        res.setFirstName(resultSet.getString(FIRST_NAME));
        res.setLastName(resultSet.getString(LAST_NAME));
      }
      return res;
    });
  }

  public List<UserProfile> getProfileList(int offset, int limit, String order) {
    return jdbcTemplate.execute((Connection con) -> {
      var statement = con.prepareStatement("select * from user_profile order by userid, ? limit ? offset ?");
      statement.setString(1, order);
      statement.setInt(2, limit);
      statement.setInt(3, offset);

      var resultSet = statement.executeQuery();

      List<UserProfile> res = new ArrayList<>();

      while (resultSet.next()) {
        res.add(UserProfile.builder()
          .id(resultSet.getInt(ID))
          .userId(resultSet.getInt(USER_ID))
          .firstName(resultSet.getString(FIRST_NAME))
          .lastName(resultSet.getString(LAST_NAME))
          .build());
      }
      return res;
    });
  }

  public UserProfile save(UserProfile profile) {
    Integer id = jdbcTemplate.execute((Connection con) -> {
        PreparedStatement statement = con.prepareStatement(
          "insert into user_profile (userid, firstname, lastname) values (?, ?, ?) returning id;");
        statement.setInt(1, profile.getUserId());
        statement.setString(2, profile.getFirstName());
        statement.setString(3, profile.getLastName());
        ResultSet res = statement.executeQuery();
        if (!res.next()) {
          return null;
        } else {
          return res.getInt(ID);
        }
      }
    );
    profile.setUserId(id);
    return new UserProfile(id, profile.getUserId(), profile.getFirstName(), profile.getLastName());
  }


  public UserProfile update(UserProfile profile, Integer userid) {
    jdbcTemplate.update("UPDATE user_profile set   firstname=?, lastname=? where userid=?",
      profile.getFirstName(), profile.getLastName(), userid);
    return profile;
  }

  public void delete(int id) {
    jdbcTemplate.update("delete from user_profile where id=?", id);
  }

}
