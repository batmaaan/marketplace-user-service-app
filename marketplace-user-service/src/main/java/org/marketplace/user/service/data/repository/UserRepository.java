package org.marketplace.user.service.data.repository;

import lombok.AllArgsConstructor;
import org.marketplace.user.model.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Repository
@AllArgsConstructor
public class UserRepository {

  private final JdbcTemplate jdbcTemplate;
  private static final String ID = "id";

  private static final String FIRST_NAME = "firstname";
  private static final String LAST_NAME = "lastname";
  private static final String EMAIL = "email";
  private static final String PASSWORD = "password";
  private static final String BIRTH_DATE = "birth_date";
  private static final String CITY = "city";

  public User getUserById(Integer id) {
    return jdbcTemplate.execute((Connection con) -> {
        var statement = con.prepareStatement("select * from users where id = ?");
        statement.setInt(1, id);
        var resultSet = statement.executeQuery();

        var res = new User();

        while (resultSet.next()) {
          res = User.builder()
            .id(resultSet.getInt(ID))
            .firstName(resultSet.getString(FIRST_NAME))
            .lastName(resultSet.getString(LAST_NAME))
            .email(resultSet.getString(EMAIL))
            .password(resultSet.getString(PASSWORD))
            .birthDate(resultSet.getDate(BIRTH_DATE))
            .city(resultSet.getString(CITY))
            .build();
        }
        return res;
      }
    );
  }

  public User getUserByEmail(String email) {
    return jdbcTemplate.execute((Connection con) -> {
        var statement = con.prepareStatement("select * from users where email = ?");
        statement.setString(1, email);
        var resultSet = statement.executeQuery();

        var res = new User();

        while (resultSet.next()) {
          res = User.builder()
            .id(resultSet.getInt(ID))
            .firstName(resultSet.getString(FIRST_NAME))
            .lastName(resultSet.getString(LAST_NAME))
            .email(resultSet.getString(EMAIL))
            .password(resultSet.getString(PASSWORD))
            .birthDate(resultSet.getDate(BIRTH_DATE))
            .city(resultSet.getString(CITY))
            .build();
        }
        return res;
      }

    );
  }

  public List<User> getUserList(int offset, int limit, String order) {
    return jdbcTemplate.execute((Connection con) -> {
      var statement = con.prepareStatement("select * from users order by id, ? limit ? offset ?");
      statement.setString(1, order);
      statement.setInt(2, limit);
      statement.setInt(3, offset);

      var resultSet = statement.executeQuery();

      List<User> res = new ArrayList<>();

      while (resultSet.next()) {
        res.add(User.builder()
          .id(resultSet.getInt(ID))
          .firstName(resultSet.getString(FIRST_NAME))
          .lastName(resultSet.getString(LAST_NAME))
          .email(resultSet.getString(EMAIL))
          .password(resultSet.getString(PASSWORD))
          .birthDate(resultSet.getDate(BIRTH_DATE))
          .city(resultSet.getString(CITY))
          .build());
      }
      return res;


    });
  }

  public User save(User user) {
    Integer id = jdbcTemplate.execute((Connection con) -> {
        PreparedStatement statement = con.prepareStatement(
          "insert into users (firstname, lastname, email, password, birth_date, city) values (?, ?,?,?,?, ?) returning id;");
        statement.setString(1, user.getFirstName());
        statement.setString(2, user.getLastName());
        statement.setString(3, user.getEmail());
        statement.setString(4, user.getPassword());
        statement.setDate(5, new java.sql.Date(user.getBirthDate().getTime()));
        statement.setString(6, user.getCity());
        ResultSet res = statement.executeQuery();
        if (!res.next()) {
          return null;
        } else {
          return res.getInt(ID);
        }
      }
    );
    return new User(id, user.getFirstName(), user.getLastName(), user.getEmail(), user.getPassword(), user.getBirthDate(), user.getCity());
  }

  public User update(User user, Integer id) {
    jdbcTemplate.update("UPDATE users set firstname=?, lastname=?, email=?, password=?, birth_date=?, city=? where id=?",
      user.getFirstName(), user.getLastName(), user.getEmail(), user.getPassword(), user.getBirthDate(), user.getCity(), id);
    return user;
  }

  public void delete(int id) {
    jdbcTemplate.update("delete from users where id=?", id);
  }

}
