package org.marketplace.user.service.data.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.marketplace.user.model.request.UserLoginRequest;
import org.marketplace.user.model.response.InformationResponse;
import org.marketplace.user.model.response.UserResponse;
import org.marketplace.user.service.data.repository.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@AllArgsConstructor
@Slf4j
public class AuthService {
  public static final String USER_NOT_FOUND = "user not found";
  public static final String ERROR = "error";
  public static final String INSERT_MAIL = "insert email please";
  public static final String INSERT_PASS = "insert pass please";
  public static final String OK = "ok";
  public static final String USER_LOGGED = "user Logged";

  private final PasswordEncoder passwordEncoder;
  private final UserRepository userRepository;

  public InformationResponse login(UserLoginRequest loginRequest) {
    if (loginRequest.getEmail() == null) {
      return new InformationResponse(ERROR, INSERT_MAIL, null);
    }
    if (loginRequest.getPassword() == null) {
      return new InformationResponse(ERROR, INSERT_PASS, null);
    }
    var user = userRepository.getUserByEmail(loginRequest.getEmail());

    if (user != null) {
      if (!Objects.equals(user.getEmail(), loginRequest.getEmail())) {
        return new InformationResponse(ERROR, USER_NOT_FOUND, null);

      }
      boolean matches = passwordEncoder.matches(loginRequest.getPassword(), user.getPassword());
      if (matches) {
        return new InformationResponse(OK, USER_LOGGED,
          new UserResponse(user.getId(), user.getFirstName(), user.getLastName(), user.getEmail(), user.getBirthDate()));
      } else {
        return new InformationResponse(ERROR, USER_NOT_FOUND, null);
      }
    }
    return new InformationResponse(ERROR, USER_NOT_FOUND, null);

  }
}
