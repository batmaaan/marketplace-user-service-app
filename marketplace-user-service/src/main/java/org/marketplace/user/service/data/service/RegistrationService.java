package org.marketplace.user.service.data.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.marketplace.mail.client.MailServiceClient;
import org.marketplace.mail.model.MessageRequest;
import org.marketplace.user.model.User;
import org.marketplace.user.model.UserProfile;
import org.marketplace.user.model.request.UserRegistrationRequest;
import org.marketplace.user.model.response.RegistrationResponse;
import org.marketplace.user.service.data.repository.UserProfileRepository;
import org.marketplace.user.service.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.regex.Pattern;

@Service
@AllArgsConstructor
@Slf4j
public class RegistrationService {
  public static final String REGEX_MAIL = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
  public static final String REGEX_PASS = "(?=.*?[A-Z])(?=.*?[0-9]).{8,}";
  public static final String ERROR = "error";
  private final PasswordEncoder passwordEncoder;
  private final UserRepository userRepository;
  private final UserProfileRepository profileRepository;
  private TransactionTemplate transactionTemplate;
   MailServiceClient mailServiceClient;

  public RegistrationResponse registration(UserRegistrationRequest registration) {

    if (registration.getEmail() == null) {
      return new RegistrationResponse(ERROR, "mail must be not empty", null);
    }
    if (!Pattern.compile(REGEX_MAIL, Pattern.CASE_INSENSITIVE).matcher(registration.getEmail()).matches()) {
      return new RegistrationResponse(ERROR, " not valid mail", null);
    }
    if (registration.getFirstName() == null) {
      return new RegistrationResponse(ERROR, "firstName must be not empty", null);
    }
    if (registration.getLastName() == null) {
      return new RegistrationResponse(ERROR, "lastName must be not empty", null);
    }
    if (registration.getBirthDate() == null) {
      return new RegistrationResponse(ERROR, "birthDate must be not empty", null);
    }
    if (registration.getPassword() == null) {
      return new RegistrationResponse(ERROR, "pass must be not empty", null);
    }
    if (!Pattern.compile(REGEX_PASS).matcher(registration.getPassword()).matches()) {
      return new RegistrationResponse(ERROR, " not valid password", null);
    }
    var user = new User();
    user.setFirstName(registration.getFirstName());
    user.setLastName(registration.getLastName());
    user.setEmail(registration.getEmail());
    user.setPassword(passwordEncoder.encode(registration.getPassword()));
    user.setBirthDate(registration.getBirthDate());

    return transactionTemplate.execute(callback -> {
      var savedUser = userRepository.save(user);

      profileRepository.save(new UserProfile(null, savedUser.getId(), savedUser.getFirstName(), savedUser.getLastName()));
      var message = new MessageRequest(
        user.getEmail(),
        "Registration is done",
        "Account " + user.getFirstName() + " is created!");

      try {
        mailServiceClient.sendMessage(message);
      } catch (Exception e) {
        log.error("send mail error");
      }
      return new RegistrationResponse("ok", "user registration is done", savedUser);
    });


  }

}
