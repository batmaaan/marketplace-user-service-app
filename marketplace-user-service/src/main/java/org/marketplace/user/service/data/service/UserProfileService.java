package org.marketplace.user.service.data.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.marketplace.user.model.UserProfile;
import org.marketplace.user.model.request.UpdateProfileRequest;
import org.marketplace.user.model.response.ProfileResponse;
import org.marketplace.user.service.data.repository.UserProfileRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class UserProfileService {
  private final UserProfileRepository userProfileRepository;

  public UserProfile getProfileById(Integer id) {
    if (id == 0 || id <= 0) {
      throw new RuntimeException("id must be more than zero");
    }
    return userProfileRepository.getProfileById(id);
  }

  public UserProfile getProfileByUserId(Integer userId) {
    if (userId == 0 || userId <= 0) {
      throw new RuntimeException("userId must be more than zero");
    }
    return userProfileRepository.getProfileByUserId(userId);
  }

  public List<UserProfile> profileList(int offset, int limit, String order) {
    return userProfileRepository.getProfileList(offset, limit, order);
  }

  public UserProfile save(UserProfile profile) {
    UserProfile saved;
    if (profile.getId() != null) {
      saved = userProfileRepository.update(profile, profile.getId());
    } else {
      saved = userProfileRepository.save(profile);
    }
    return saved;
  }

  public Map<String, String> delete(int id) {
    try {
      userProfileRepository.delete(id);
      return Map.of("Status", "ok");
    } catch (Exception e) {
      log.info("delete", e);
      return Map.of("Status", "error");
    }
  }

  public ProfileResponse update(Integer userId, UpdateProfileRequest profileRequest) {

    var userProfile = new UserProfile();
    userProfile.setUserId(profileRequest.getUserId());
    userProfile.setFirstName(profileRequest.getFirstName());
    userProfile.setLastName(profileRequest.getLastName());


    userProfileRepository.update(userProfile, userId);

    return ProfileResponse.fromProfile(userProfile);
  }
}
