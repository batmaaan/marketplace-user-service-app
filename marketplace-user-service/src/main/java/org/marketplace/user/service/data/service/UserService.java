package org.marketplace.user.service.data.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.marketplace.user.model.User;
import org.marketplace.user.service.data.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class UserService {
  private final UserRepository userRepository;

  public User getUserById(Integer id) {
    if (id == 0 || id <= 0) {
      throw new RuntimeException("id must be more than zero");
    }
    return userRepository.getUserById(id);
  }

  public User getUserByEmail(String mail) {
    if (mail == null || mail.equals("")) {
      throw new RuntimeException("email bust be correct");
    } else {
      return userRepository.getUserByEmail(mail);
    }
  }

  public List<User> listUser(int offset, int limit, String order) {
    return userRepository.getUserList(offset, limit, order);
  }

  public User saveAndReturnUser(User user) {
    User saved;
    if (user.getId() == null) {
      saved = userRepository.save(user);
    } else {
      saved = userRepository.update(user, user.getId());
    }
    return saved;
  }

  public Map<String, String> delete(int id) {
    try {
      userRepository.delete(id);
      return Map.of("Status", "ok");
    } catch (Exception e) {
      log.info("delete", e);
      return Map.of("Status", "error");
    }
  }
}




