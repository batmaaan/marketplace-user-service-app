create sequence user_profile_id_seq;
create table user_profile

(
  id        integer default nextval('user_profile_id_seq'::regclass) not null
    primary key,
  userId    integer,
  firstname varchar(500)                                             not null,
  lastname  varchar(500)                                             not null
);
