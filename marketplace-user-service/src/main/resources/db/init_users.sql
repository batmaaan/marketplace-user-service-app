create sequence users_id_seq;
create table users

(
  id        integer default nextval('users_id_seq'::regclass) not null
    primary key,
  firstName varchar(100)                                      not null,
  lastName  varchar(100)                                      not null,
  email     varchar                                           not null,
  password  varchar,
  birth_date       timestamp,
  city      varchar
);
