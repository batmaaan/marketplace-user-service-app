package org.marketplace.user.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;

@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(
  initializers = {AbstractIntegrationTestForUser.Initializer.class
}
)

public abstract class AbstractIntegrationTestForUser {
@Container
  public static final PostgreSQLContainer<?> POSTGRES = new PostgreSQLContainer<>("postgres:12")
    .withDatabaseName("databasename")
    .withUsername("sa")
    .withPassword("sa")
    .withInitScript("db/init_test_db.sql")
    .withExposedPorts(5432);


  static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    public void initialize(ConfigurableApplicationContext applicationContext) {
      POSTGRES.start();

      var testPropertyValues = TestPropertyValues.of(
        "app.datasource.url=" + POSTGRES.getJdbcUrl(),
        "app.datasource.username=" + POSTGRES.getUsername(),
        "app.datasource.password=" + POSTGRES.getPassword()
      );
      testPropertyValues.applyTo(applicationContext.getEnvironment());
    }
  }
}


