package org.marketplace.user.service.api;


import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.marketplace.user.model.request.UserLoginRequest;
import org.marketplace.user.model.response.InformationResponse;
import org.marketplace.user.model.response.UserResponse;
import org.marketplace.user.service.AbstractIntegrationTestForUser;
import org.marketplace.user.service.data.repository.UserRepository;
import org.marketplace.utils.InitUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.assertEquals;


@Slf4j
class AuthControllerTest extends AbstractIntegrationTestForUser {
  @LocalServerPort
  int randomServerPort;

  private String BASE_URL;

  OkHttpClient client = new OkHttpClient();
  @Autowired
  private PasswordEncoder passwordEncoder;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private JdbcTemplate jdbcTemplate;
  ObjectMapper objectMapper = new ObjectMapper();

  public static final MediaType JSON = MediaType.parse("application/json");

  @BeforeEach
  public void init() {
    if (BASE_URL == null) {
      BASE_URL = "http://localhost:" + randomServerPort + "/marketplace/user/service/auth/login";
    }
  }

  @AfterEach
  public void clearTable() {
    jdbcTemplate.execute("DELETE FROM users");
  }

  @Test
  void login() throws IOException {
    var randomUser = InitUser.initRandomUser();
    var pass = randomUser.getPassword();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    var user = userRepository.save(randomUser);
    UserLoginRequest logRequest = new UserLoginRequest(randomUser.getEmail(), pass);
    String json = objectMapper.writeValueAsString(logRequest);

    var goodResponce = new InformationResponse(
      "ok",
      "user Logged",
      new UserResponse(user.getId(),
        user.getFirstName(),
        user.getLastName(),
        user.getEmail(),
        user.getBirthDate()
      )
    );
    RequestBody body = RequestBody.create(
      json, JSON);

    Request request = new Request.Builder()
      .url(BASE_URL)
      .post(body)
      .build();

    try (Response response = client.newCall(request).execute()) {
      assert response.body() != null;
      var responceForAssert = objectMapper.readValue(response.body().string(), InformationResponse.class);
      SimpleDateFormat dateFormat = new SimpleDateFormat("MMMMM dd, yyyy");

      assertEquals(200, response.code());
      assertEquals(goodResponce.getResult().getId(), responceForAssert.getResult().getId());
      assertEquals(goodResponce.getResult().getFirstName(), responceForAssert.getResult().getFirstName());
      assertEquals(goodResponce.getResult().getLastName(), responceForAssert.getResult().getLastName());
      assertEquals(goodResponce.getResult().getEmail(), responceForAssert.getResult().getEmail());
      assertEquals(
        dateFormat.format(goodResponce.getResult().getBirthDate().getTime()),
        dateFormat.format(responceForAssert.getResult().getBirthDate()
        )
      );

    }catch (Exception e){
      log.error("auth: ", e);
      throw new RuntimeException(e);
    }
  }


  @Test
  void loginWithoutEmail() throws IOException {
    var randomUser = InitUser.initRandomUser();
    var pass = randomUser.getPassword();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    userRepository.save(randomUser);
    UserLoginRequest logRequest = new UserLoginRequest(null, pass);
    var errorResponce = new InformationResponse("error", "insert email please", null);
    String json = objectMapper.writeValueAsString(logRequest);
    RequestBody body = RequestBody.create(
      json, JSON);

    Request request = new Request.Builder()
      .url(BASE_URL)
      .post(body)
      .build();

    try (Response response = client.newCall(request).execute()) {

      assert response.body() != null;
      var responceForAssert = objectMapper.readValue(response.body().string(), InformationResponse.class);
      assertEquals(200, response.code());
      assertEquals(errorResponce, responceForAssert);
    }catch (Exception e){
      log.error("auth: ", e);
      throw new RuntimeException(e);
    }
  }

  @Test
  void loginWithoutPass() throws IOException {
    var randomUser = InitUser.initRandomUser();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    userRepository.save(randomUser);
    UserLoginRequest logRequest = new UserLoginRequest(randomUser.getEmail(), null);
    var errorResponce = new InformationResponse("error", "insert pass please", null);
    String json = objectMapper.writeValueAsString(logRequest);
    RequestBody body = RequestBody.create(
      json, JSON);

    Request request = new Request.Builder()
      .url(BASE_URL)
      .post(body)
      .build();

    try (Response response = client.newCall(request).execute()) {

      assert response.body() != null;
      var responceForAssert = objectMapper.readValue(response.body().string(), InformationResponse.class);
      assertEquals(200, response.code());
      assertEquals(errorResponce, responceForAssert);
    }catch (Exception e){
      log.error("auth: ", e);
      throw new RuntimeException(e);
    }
  }

  @Test
  void loginWithWrongPass() throws IOException {
    var randomUser = InitUser.initRandomUser();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    userRepository.save(randomUser);
    UserLoginRequest logRequest = new UserLoginRequest(randomUser.getEmail(), "dslkjfldksjflksd");
    var errorResponce = new InformationResponse("error", "user not found", null);
    String json = objectMapper.writeValueAsString(logRequest);
    RequestBody body = RequestBody.create(
      json, JSON);

    Request request = new Request.Builder()
      .url(BASE_URL)
      .post(body)
      .build();

    try (Response response = client.newCall(request).execute()) {

      assert response.body() != null;
      var responceForAssert = objectMapper.readValue(response.body().string(), InformationResponse.class);
      assertEquals(200, response.code());
      assertEquals(errorResponce, responceForAssert);
    }catch (Exception e){
      log.error("auth: ", e);
      throw new RuntimeException(e);
    }
  }

  @Test
  void loginWithWrongEmail() throws IOException {
    var randomUser = InitUser.initRandomUser();
    var pass = randomUser.getPassword();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    userRepository.save(randomUser);
    UserLoginRequest logRequest = new UserLoginRequest("jsdhfjksdh", pass);
    var errorResponce = new InformationResponse("error", "user not found", null);
    String json = objectMapper.writeValueAsString(logRequest);
    RequestBody body = RequestBody.create(
      json, JSON);

    Request request = new Request.Builder()
      .url(BASE_URL)
      .post(body)
      .build();

    try (Response response = client.newCall(request).execute()) {

      assert response.body() != null;
      var responceForAssert = objectMapper.readValue(response.body().string(), InformationResponse.class);
      assertEquals(200, response.code());
      assertEquals(errorResponce, responceForAssert);
    }catch (Exception e){
      log.error("auth: ", e);
      throw new RuntimeException(e);
    }
  }

  @Test
  void loginWithWrongEmailAndPass() throws IOException {
    var randomUser = InitUser.initRandomUser();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    userRepository.save(randomUser);
    UserLoginRequest logRequest = new UserLoginRequest("jsdhfjksdh", "sjdhfjksd");
    var errorResponce = new InformationResponse("error", "user not found", null);
    String json = objectMapper.writeValueAsString(logRequest);
    RequestBody body = RequestBody.create(
      json, JSON);

    Request request = new Request.Builder()
      .url(BASE_URL)
      .post(body)
      .build();

    try (Response response = client.newCall(request).execute()) {

      assert response.body() != null;
      var responceForAssert = objectMapper.readValue(response.body().string(), InformationResponse.class);
      assertEquals(200, response.code());
      assertEquals(errorResponce, responceForAssert);
    }catch (Exception e){
      log.error("auth: ", e);
      throw new RuntimeException(e);
    }
  }
}

