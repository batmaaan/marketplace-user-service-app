package org.marketplace.user.service.api;


import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.marketplace.user.model.request.UserRegistrationRequest;
import org.marketplace.user.model.response.RegistrationResponse;
import org.marketplace.user.service.AbstractIntegrationTestForUser;
import org.marketplace.utils.InitUser;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.io.IOException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@Slf4j
class RegistrationControllerTest extends AbstractIntegrationTestForUser {

  @LocalServerPort
  int randomServerPort;
  private String BASE_URL;

  public static final MediaType JSON = MediaType.parse("application/json");
  ObjectMapper objectMapper = new ObjectMapper();
  OkHttpClient client = new OkHttpClient();

  @BeforeEach
  public void init() {
    if (BASE_URL == null) {
      BASE_URL = "http://localhost:" + randomServerPort + "/marketplace/user/service/auth/registration";
    }
  }

  @Test
  void registration() throws IOException {
    var randomUser = InitUser.initRandomUser();
    var newUser = new UserRegistrationRequest(
      randomUser.getFirstName(),
      randomUser.getLastName(),
      randomUser.getEmail(),
      randomUser.getPassword(),
      randomUser.getBirthDate());
    String json = objectMapper.writeValueAsString(newUser);

    var goodResponce = new RegistrationResponse(
      "ok",
      "user registration is done", null);

    RequestBody body = RequestBody.create(
      json, JSON);
    Request request = new Request.Builder()
      .url(BASE_URL)
      .post(body)
      .build();

    try (Response response = client.newCall(request).execute()) {
      SimpleDateFormat dateFormat = new SimpleDateFormat("MMMMM dd, yyyy");
      assert response.body() != null;
      var responseForAssert = objectMapper.readValue(response.body().string(), RegistrationResponse.class);
      assertEquals(200, response.code());
      assertEquals("ok", responseForAssert.getStatus());

    } catch (Exception e) {
      log.error("login: ", e);
      throw new RuntimeException(e);
    }
  }

  @Test
  void registrationWithWrongMail() throws IOException {
    var randomUser = InitUser.initRandomUser();
    var newUser = new UserRegistrationRequest(
      randomUser.getFirstName(),
      randomUser.getLastName(),
      "dsjhfjk",
      randomUser.getPassword(),
      randomUser.getBirthDate());
    String json = objectMapper.writeValueAsString(newUser);
    var errorResponce = new RegistrationResponse("error", " not valid mail", null);

    RequestBody body = RequestBody.create(
      json, JSON);
    Request request = new Request.Builder()
      .url(BASE_URL)
      .post(body)
      .build();

    try (Response response = client.newCall(request).execute()) {
      assert response.body() != null;
      var responseForAssert = objectMapper.readValue(response.body().string(), RegistrationResponse.class);
      assertEquals(200, response.code());
      assertEquals(errorResponce, responseForAssert);
    } catch (Exception e) {
      log.error("login: ", e);
      throw new RuntimeException(e);
    }
  }

  @Test
  void registrationWithNullMail() throws IOException {
    var randomUser = InitUser.initRandomUser();
    var newUser = new UserRegistrationRequest(
      randomUser.getFirstName(),
      randomUser.getLastName(),
      null,
      randomUser.getPassword(),
      randomUser.getBirthDate());
    String json = objectMapper.writeValueAsString(newUser);
    var errorResponce = new RegistrationResponse("error", "mail must be not empty", null);

    RequestBody body = RequestBody.create(
      json, JSON);
    Request request = new Request.Builder()
      .url(BASE_URL)
      .post(body)
      .build();

    try (Response response = client.newCall(request).execute()) {
      assert response.body() != null;
      var responceForAssert = objectMapper.readValue(response.body().string(), RegistrationResponse.class);
      assertEquals(200, response.code());
      assertEquals(errorResponce, responceForAssert);
    } catch (Exception e) {
      log.error("login: ", e);
      throw new RuntimeException(e);
    }
  }

  @Test
  void registrationWithWrongPass() throws IOException {
    var randomUser = InitUser.initRandomUser();
    var newUser = new UserRegistrationRequest(
      randomUser.getFirstName(),
      randomUser.getLastName(),
      randomUser.getEmail(),
      "hhh",
      randomUser.getBirthDate());
    String json = objectMapper.writeValueAsString(newUser);
    var errorResponce = new RegistrationResponse("error", " not valid password", null);

    RequestBody body = RequestBody.create(
      json, JSON);
    Request request = new Request.Builder()
      .url(BASE_URL)
      .post(body)
      .build();

    try (Response response = client.newCall(request).execute()) {
      assert response.body() != null;
      var responceForAssert = objectMapper.readValue(response.body().string(), RegistrationResponse.class);
      assertEquals(200, response.code());
      assertEquals(errorResponce, responceForAssert);
    } catch (Exception e) {
      log.error("login: ", e);
      throw new RuntimeException(e);
    }
  }

  @Test
  void registrationWithNullPass() throws IOException {
    var randomUser = InitUser.initRandomUser();
    var newUser = new UserRegistrationRequest(
      randomUser.getFirstName(),
      randomUser.getLastName(),
      randomUser.getEmail(),
      null,
      randomUser.getBirthDate());
    String json = objectMapper.writeValueAsString(newUser);
    var errorResponce = new RegistrationResponse("error", "pass must be not empty", null);
    RequestBody body = RequestBody.create(
      json, JSON);
    Request request = new Request.Builder()
      .url(BASE_URL)
      .post(body)
      .build();

    try (Response response = client.newCall(request).execute()) {
      assert response.body() != null;
      var responceForAssert = objectMapper.readValue(response.body().string(), RegistrationResponse.class);
      assertEquals(200, response.code());
      assertEquals(errorResponce, responceForAssert);
    } catch (Exception e) {
      log.error("login: ", e);
      throw new RuntimeException(e);
    }
  }

  @Test
  void registrationWithoutFirstName() throws IOException {
    var randomUser = InitUser.initRandomUser();
    var newUser = new UserRegistrationRequest(
      null,
      randomUser.getLastName(),
      randomUser.getEmail(),
      randomUser.getPassword(),
      randomUser.getBirthDate());
    String json = objectMapper.writeValueAsString(newUser);
    var errorResponce = new RegistrationResponse("error", "firstName must be not empty", null);
    RequestBody body = RequestBody.create(
      json, JSON);
    Request request = new Request.Builder()
      .url(BASE_URL)
      .post(body)
      .build();

    try (Response response = client.newCall(request).execute()) {
      assert response.body() != null;
      var responceForAssert = objectMapper.readValue(response.body().string(), RegistrationResponse.class);
      assertEquals(200, response.code());
      assertEquals(errorResponce, responceForAssert);
    } catch (Exception e) {
      log.error("login: ", e);
      throw new RuntimeException(e);
    }
  }

  @Test
  void registrationWithoutLastName() throws IOException {
    var randomUser = InitUser.initRandomUser();
    var newUser = new UserRegistrationRequest(
      randomUser.getFirstName(),
      null,
      randomUser.getEmail(),
      randomUser.getPassword(),
      randomUser.getBirthDate());
    String json = objectMapper.writeValueAsString(newUser);
    var errorResponce = new RegistrationResponse("error", "lastName must be not empty", null);
    RequestBody body = RequestBody.create(
      json, JSON);
    Request request = new Request.Builder()
      .url(BASE_URL)
      .post(body)
      .build();

    try (Response response = client.newCall(request).execute()) {
      assert response.body() != null;
      var responceForAssert = objectMapper.readValue(response.body().string(), RegistrationResponse.class);
      assertEquals(200, response.code());
      assertEquals(errorResponce, responceForAssert);
    } catch (Exception e) {
      log.error("login: ", e);
      throw new RuntimeException(e);
    }
  }
}
