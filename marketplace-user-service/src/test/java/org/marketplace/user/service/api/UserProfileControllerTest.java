package org.marketplace.user.service.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.marketplace.user.model.UserProfile;
import org.marketplace.user.model.request.UpdateProfileRequest;
import org.marketplace.user.model.response.InformationProfileResponse;
import org.marketplace.user.service.AbstractIntegrationTestForUser;
import org.marketplace.user.service.data.service.UserProfileService;
import org.marketplace.user.service.data.service.UserService;
import org.marketplace.utils.InitUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
class UserProfileControllerTest extends AbstractIntegrationTestForUser {
  @Autowired
  private UserProfileService userProfileService;
  @Autowired
  private PasswordEncoder passwordEncoder;
  @Autowired
  private UserService userService;
  @Autowired
  private JdbcTemplate jdbcTemplate;

  public static final MediaType JSON = MediaType.parse("application/json");


  @LocalServerPort
  int randomServerPort;
  private String BASE_URL;

  ObjectMapper objectMapper = new ObjectMapper();
  OkHttpClient client = new OkHttpClient();

  @BeforeEach
  public void init() {
    if (BASE_URL == null) {
      BASE_URL = "http://localhost:" + randomServerPort + "/marketplace/user/profile";
    }
  }

  @AfterEach
  public void clearTable() {
    jdbcTemplate.execute("DELETE FROM user_profile");
  }

  @Test
  void getProfile() {
    var randomUser = InitUser.initRandomUser();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    var newUser = userService.saveAndReturnUser(randomUser);
    var newProfile = userProfileService.save(
      new UserProfile(
        null,
        newUser.getId(),
        newUser.getFirstName(),
        newUser.getLastName()
      ));
    Request request = new Request.Builder()
      .url(BASE_URL + "/profile?userId=" + newProfile.getUserId())
      .build();

    try (Response response = client.newCall(request).execute()) {
      assert response.body() != null;
      var responceForAssert = objectMapper.readValue(response.body().string(), InformationProfileResponse.class);

      assertEquals(200, response.code());
      assertEquals("ok", responceForAssert.getStatus());
      assertEquals("", responceForAssert.getDescription());
      assertEquals(newProfile.getUserId(), responceForAssert.getResult().getUserId());
      assertEquals(newProfile.getFirstName(), responceForAssert.getResult().getFirstName());
      assertEquals(newProfile.getLastName(), responceForAssert.getResult().getLastName());
    } catch (Exception e) {
      log.error("profile error: ", e);
      throw new RuntimeException(e);
    }
  }

  @Test
  void updateProfile() throws IOException {
    var randomUser = InitUser.initRandomUser();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    var newUser = userService.saveAndReturnUser(randomUser);
    var randomForUpdate = InitUser.initRandomProfile();
    var newProfile = userProfileService.save(
      new UserProfile(null, newUser.getId(), newUser.getFirstName(), newUser.getLastName()));
    var profileRequest = new UpdateProfileRequest(
      newProfile.getUserId(),
      randomForUpdate.getFirstName(),
      randomForUpdate.getLastName()
    );

    String json = objectMapper.writeValueAsString(profileRequest);

    RequestBody body = RequestBody.create(
      json, JSON);

    Request request = new Request.Builder()
      .url(BASE_URL + "/update")
      .post(body)
      .build();

    try (Response response = client.newCall(request).execute()) {
      assert response.body() != null;
      var responceForAssert = objectMapper.readValue(response.body().string(), InformationProfileResponse.class);

      assertEquals(200, response.code());
      assertEquals("ok", responceForAssert.getStatus());
      assertEquals("profile updated", responceForAssert.getDescription());
      assertEquals(newProfile.getUserId(), responceForAssert.getResult().getUserId());
      assertEquals(randomForUpdate.getFirstName(), responceForAssert.getResult().getFirstName());
      assertEquals(randomForUpdate.getLastName(), responceForAssert.getResult().getLastName());
    } catch (Exception e) {
      log.error("profile error: ", e);
      throw new RuntimeException(e);
    }
  }


}

