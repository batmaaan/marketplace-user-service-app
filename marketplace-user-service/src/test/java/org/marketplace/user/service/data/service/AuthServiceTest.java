package org.marketplace.user.service.data.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.marketplace.user.service.AbstractIntegrationTestForUser;
import org.marketplace.user.service.data.repository.UserRepository;
import org.marketplace.utils.InitUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.marketplace.user.model.request.UserLoginRequest;
import org.marketplace.user.model.response.InformationResponse;

import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@Slf4j
class AuthServiceTest extends AbstractIntegrationTestForUser {
  @Autowired
  private AuthService authService;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private JdbcTemplate jdbcTemplate;
  @Autowired
  private PasswordEncoder passwordEncoder;


  @AfterEach
  public void clearTable() {
    jdbcTemplate.execute("DELETE FROM users");
  }

  @Test
  void login() {
    var randomUser = InitUser.initRandomUser();
    var pass = randomUser.getPassword();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    var user = userRepository.save(randomUser);
    UserLoginRequest request = new UserLoginRequest(randomUser.getEmail(), pass);
    InformationResponse loggedUser = authService.login(request);
    SimpleDateFormat dateFormat = new SimpleDateFormat("MMMMM dd, yyyy");

    assertEquals("ok", loggedUser.getStatus());
    assertEquals(user.getId(), loggedUser.getResult().getId());
    assertEquals(user.getFirstName(), loggedUser.getResult().getFirstName());
    assertEquals(user.getLastName(), loggedUser.getResult().getLastName());
    assertEquals(user.getEmail(), loggedUser.getResult().getEmail());
    assertEquals(
      dateFormat.format(user.getBirthDate().getTime()),
      dateFormat.format(loggedUser.getResult().getBirthDate().getTime())
    );
  }

  @Test
  void loginWithoutEmail() {
    var randomUser = InitUser.initRandomUser();
    var pass = randomUser.getPassword();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    userRepository.save(randomUser);
    UserLoginRequest request = new UserLoginRequest(null, pass);
    InformationResponse loggedUser = authService.login(request);

    assertEquals("error", loggedUser.getStatus());
    assertEquals("insert email please", loggedUser.getDescription());
    assertNull(loggedUser.getResult());
  }

  @Test
  void loginWithoutPass() {
    var randomUser = InitUser.initRandomUser();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    userRepository.save(randomUser);
    UserLoginRequest request = new UserLoginRequest(randomUser.getEmail(), null);
    InformationResponse loggedUser = authService.login(request);

    assertEquals("error", loggedUser.getStatus());
    assertEquals("insert pass please", loggedUser.getDescription());
    assertNull(loggedUser.getResult());
  }

  @Test
  void loginWithWrongPass() {
    var randomUser = InitUser.initRandomUser();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    userRepository.save(randomUser);
    UserLoginRequest request = new UserLoginRequest(randomUser.getEmail(), "kjsdhfjksdfh");
    InformationResponse loggedUser = authService.login(request);

    assertEquals("error", loggedUser.getStatus());
    assertEquals("user not found", loggedUser.getDescription());
    assertNull(loggedUser.getResult());
  }

  @Test
  void loginWithWrongEmail() {
    var randomUser = InitUser.initRandomUser();
    var pass = randomUser.getPassword();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    userRepository.save(randomUser);
    UserLoginRequest request = new UserLoginRequest("jjhh", pass);
    InformationResponse loggedUser = authService.login(request);

    assertEquals("error", loggedUser.getStatus());
    assertEquals("user not found", loggedUser.getDescription());
    assertNull(loggedUser.getResult());

  }

  @Test
  void loginWithWrongEmailAndPass() {
    var randomUser = InitUser.initRandomUser();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    userRepository.save(randomUser);
    UserLoginRequest request = new UserLoginRequest("kkjhdfjkd", "jflksdj");
    InformationResponse loggedUser = authService.login(request);

    assertEquals("error", loggedUser.getStatus());
    assertEquals("user not found", loggedUser.getDescription());
    assertNull(loggedUser.getResult());
  }
}
