package org.marketplace.user.service.data.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.marketplace.user.service.AbstractIntegrationTestForUser;
import org.marketplace.utils.InitUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.marketplace.user.model.request.UserRegistrationRequest;

import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class RegistrationServiceTest extends AbstractIntegrationTestForUser {


  @Autowired
  private RegistrationService registrationService;


  @Test
  void registration() {
    var randomUser = InitUser.initRandomUser();
    var registration = new UserRegistrationRequest(
      randomUser.getFirstName(),
      randomUser.getLastName(),
      randomUser.getEmail(),
      randomUser.getPassword(),
      randomUser.getBirthDate());

    var newUser = registrationService.registration(registration);
    SimpleDateFormat dateFormat = new SimpleDateFormat("MMMMM dd, yyyy");
    assertEquals("ok", newUser.getStatus());
    assertEquals("user registration is done", newUser.getDescription());

  }

  @Test
  void registrationWithWrongMail() {
    var randomUser = InitUser.initRandomUser();
    var registration = new UserRegistrationRequest(
      randomUser.getFirstName(),
      randomUser.getLastName(),
      "sjflkdsfjlkfj",
      randomUser.getPassword(),
      randomUser.getBirthDate());
    var newUser = registrationService.registration(registration);
    assertEquals("error", newUser.getStatus());
    assertEquals(" not valid mail", newUser.getDescription());

  }

  @Test
  void registrationWithNullMail() {

    var randomUser = InitUser.initRandomUser();
    var registration = new UserRegistrationRequest(
      randomUser.getFirstName(),
      randomUser.getLastName(),
      null,
      randomUser.getPassword(),
      randomUser.getBirthDate());
    var newUser = registrationService.registration(registration);
    assertEquals("error", newUser.getStatus());
    assertEquals("mail must be not empty", newUser.getDescription());

  }

  @Test
  void registrationWithWrongPass() {
    var randomUser = InitUser.initRandomUser();
    var registration = new UserRegistrationRequest(
      randomUser.getFirstName(),
      randomUser.getLastName(),
      randomUser.getEmail(),
      "jhsdfjksdfh",
      randomUser.getBirthDate());
    var newUser = registrationService.registration(registration);
    assertEquals("error", newUser.getStatus());
    assertEquals(" not valid password", newUser.getDescription());

  }

  @Test
  void registrationWithNullPass() {
    var randomUser = InitUser.initRandomUser();
    var registration = new UserRegistrationRequest(
      randomUser.getFirstName(),
      randomUser.getLastName(),
      randomUser.getEmail(),
      null,
      randomUser.getBirthDate());
    var newUser = registrationService.registration(registration);
    assertEquals("error", newUser.getStatus());
    assertEquals("pass must be not empty", newUser.getDescription());

  }

  @Test
  void registrationWithoutFirstName() {
    var randomUser = InitUser.initRandomUser();
    var registration = new UserRegistrationRequest(
      null,
      randomUser.getLastName(),
      randomUser.getEmail(),
      randomUser.getPassword(),
      randomUser.getBirthDate());
    var newUser = registrationService.registration(registration);
    assertEquals("error", newUser.getStatus());
    assertEquals("firstName must be not empty", newUser.getDescription());

  }

  @Test
  void registrationWithoutLastName() {
    var randomUser = InitUser.initRandomUser();
    var registration = new UserRegistrationRequest(
      randomUser.getFirstName(),
      null,
      randomUser.getEmail(),
      randomUser.getPassword(),
      randomUser.getBirthDate());
    var newUser = registrationService.registration(registration);
    assertEquals("error", newUser.getStatus());
    assertEquals("lastName must be not empty", newUser.getDescription());

  }
}
