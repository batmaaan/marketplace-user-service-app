package org.marketplace.user.service.data.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.marketplace.user.service.AbstractIntegrationTestForUser;
import org.marketplace.user.model.UserProfile;
import org.marketplace.utils.InitUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.marketplace.user.model.request.UpdateProfileRequest;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
class UserProfileServiceTest extends AbstractIntegrationTestForUser {

  @Autowired
  private UserProfileService userProfileService;
  @Autowired
  private PasswordEncoder passwordEncoder;
  @Autowired
  private UserService userService;
  @Autowired
  private JdbcTemplate jdbcTemplate;

  @AfterEach
  public void clearTable() {
    jdbcTemplate.execute("DELETE FROM user_profile");
  }


  @Test
  void getProfileById() {
    var randomUser = InitUser.initRandomUser();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    var newUser = userService.saveAndReturnUser(randomUser);
    var newProfile = userProfileService.save(new UserProfile(
      null,
      newUser.getId(),
      newUser.getFirstName(),
      newUser.getLastName()
    ));

    var profileById = userProfileService.getProfileById(newProfile.getId());

    assertEquals(profileById.getId(), newProfile.getId());
    assertEquals(profileById.getUserId(), newProfile.getUserId());
    assertEquals(profileById.getFirstName(), newProfile.getFirstName());
    assertEquals(profileById.getLastName(), newProfile.getLastName());
  }

  @Test
  void getProfileByUserId() {
    var randomUser = InitUser.initRandomUser();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    var newUser = userService.saveAndReturnUser(randomUser);
    var newProfile = userProfileService.save(new UserProfile(
      null,
      newUser.getId(),
      newUser.getFirstName(),
      newUser.getLastName()
    ));

    var profileById = userProfileService.getProfileByUserId(newProfile.getUserId());

    assertEquals(profileById.getId(), newProfile.getId());
    assertEquals(profileById.getUserId(), newProfile.getUserId());
    assertEquals(profileById.getFirstName(), newProfile.getFirstName());
    assertEquals(profileById.getLastName(), newProfile.getLastName());
  }

  @Test
  void profileList() {
    var randomUser1 = InitUser.initRandomUser();
    randomUser1.setPassword(passwordEncoder.encode(randomUser1.getPassword()));
    var newUser1 = userService.saveAndReturnUser(randomUser1);
    var newProfile1 = userProfileService.save(
      new UserProfile(null, newUser1.getId(), newUser1.getFirstName(), newUser1.getLastName()
      )
    );
    var randomUser2 = InitUser.initRandomUser();
    randomUser2.setPassword(passwordEncoder.encode(randomUser2.getPassword()));
    var newUser2 = userService.saveAndReturnUser(randomUser2);
    var newProfile2 = userProfileService.save(
      new UserProfile(null, newUser2.getId(), newUser2.getFirstName(), newUser2.getLastName()
      )
    );
    var randomUser3 = InitUser.initRandomUser();
    randomUser3.setPassword(passwordEncoder.encode(randomUser1.getPassword()));
    var newUser3 = userService.saveAndReturnUser(randomUser3);
    var newProfile3 = userProfileService.save(
      new UserProfile(null, newUser3.getId(), newUser3.getFirstName(), newUser3.getLastName()
      )
    );
    var profileList = List.of(newProfile1, newProfile2, newProfile3);
    var getProfileList = userProfileService.profileList(0, 3, "ASC");
    assertEquals(getProfileList, profileList);
  }

  @Test
  void save() {
    var randomUser = InitUser.initRandomUser();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    var newUser = userService.saveAndReturnUser(randomUser);
    var newProfile = userProfileService.save(
      new UserProfile(null, newUser.getId(), newUser.getFirstName(), newUser.getLastName()));

    assertNotNull(newProfile.getId());
    assertEquals(newProfile.getUserId(), newUser.getId());
    assertEquals(newProfile.getFirstName(), newUser.getFirstName());
    assertEquals(newProfile.getLastName(), newUser.getLastName());
  }

  @Test
  void delete() {
    var randomUser = InitUser.initRandomUser();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    var newUser = userService.saveAndReturnUser(randomUser);
    var newProfile = userProfileService.save(
      new UserProfile(null, newUser.getId(), newUser.getFirstName(), newUser.getLastName()));

    var deleteProfile = userProfileService.delete(newProfile.getId());
    assertEquals(Map.of("Status", "ok"), deleteProfile);
  }

  @Test
  void update() {
    var randomUser = InitUser.initRandomUser();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    var newUser = userService.saveAndReturnUser(randomUser);
    var randomForUpdate = InitUser.initRandomProfile();
    var newProfile = userProfileService.save(
      new UserProfile(null, newUser.getId(), newUser.getFirstName(), newUser.getLastName()));

    var updatedUser = userProfileService.update(
      newProfile.getId(),
      new UpdateProfileRequest(
        newProfile.getUserId(),
        randomForUpdate.getFirstName(),
        randomForUpdate.getLastName()
      )
    );
    assertEquals(randomForUpdate.getFirstName(), updatedUser.getFirstName());
    assertEquals(randomForUpdate.getLastName(), updatedUser.getLastName());
  }
}
