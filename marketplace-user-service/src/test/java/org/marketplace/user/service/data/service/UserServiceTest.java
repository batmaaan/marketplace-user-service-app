package org.marketplace.user.service.data.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.marketplace.user.service.AbstractIntegrationTestForUser;
import org.marketplace.user.service.data.service.UserService;
import org.marketplace.utils.InitUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.text.SimpleDateFormat;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceTest extends AbstractIntegrationTestForUser {

  @Autowired
  private UserService userService;
  @Autowired
  private JdbcTemplate jdbcTemplate;
  @Autowired
  private PasswordEncoder passwordEncoder;

  @AfterEach
  public void clearTable() {
    jdbcTemplate.execute("DELETE FROM users");
  }


  @Test
  void getUserById() {
    var randomUser = InitUser.initRandomUser();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    var newUser = userService.saveAndReturnUser(randomUser);
    var userById = userService.getUserById(newUser.getId());
    SimpleDateFormat dateFormat = new SimpleDateFormat("MMMMM dd, yyyy");

    assertEquals(userById.getId(), newUser.getId());
    assertEquals(userById.getFirstName(), newUser.getFirstName());
    assertEquals(userById.getLastName(), newUser.getLastName());
    assertEquals(userById.getEmail(), newUser.getEmail());
    assertEquals(userById.getPassword(), newUser.getPassword());
    assertEquals(
      dateFormat.format(userById.getBirthDate().getTime()),
      dateFormat.format(newUser.getBirthDate().getTime())
    );
    assertEquals(userById.getCity(), newUser.getCity());
  }


  @Test
  void getUserWithNullId() {
    assertThrows(Exception.class,
      () -> {
        var randomUser = InitUser.initRandomUser();
        randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
        var newUser = userService.saveAndReturnUser(randomUser);
        var userById = userService.getUserById(null);
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMMM dd, yyyy");

        assertEquals(userById.getId(), newUser.getId());
        assertEquals(userById.getFirstName(), newUser.getFirstName());
        assertEquals(userById.getLastName(), newUser.getLastName());
        assertEquals(userById.getEmail(), newUser.getEmail());
        assertEquals(userById.getPassword(), newUser.getPassword());
        assertEquals(
          dateFormat.format(userById.getBirthDate().getTime()),
          dateFormat.format(newUser.getBirthDate().getTime())
        );
        assertEquals(userById.getCity(), newUser.getCity());
      }
    );
  }

  @Test
  void saveAndReturnUser() {
    var randomUser = InitUser.initRandomUser();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    var newUser = userService.saveAndReturnUser(randomUser);

    SimpleDateFormat dateFormat = new SimpleDateFormat("MMMMM dd, yyyy");

    assertNotNull(newUser.getId());
    assertEquals(newUser.getFirstName(), newUser.getFirstName());
    assertEquals(newUser.getLastName(), newUser.getLastName());
    assertEquals(newUser.getEmail(), newUser.getEmail());
    assertEquals(newUser.getPassword(), newUser.getPassword());
    assertEquals(
      dateFormat.format(newUser.getBirthDate().getTime()),
      dateFormat.format(newUser.getBirthDate().getTime())
    );
    assertEquals(newUser.getCity(), newUser.getCity());
  }

  @Test
  void delete() {

    var randomUser = InitUser.initRandomUser();
    randomUser.setPassword(passwordEncoder.encode(randomUser.getPassword()));
    var newUser = userService.saveAndReturnUser(randomUser);
    var deleteMessage = userService.delete(newUser.getId());
    assertEquals(Map.of("Status", "ok"), deleteMessage);
  }

}
