package org.marketplace.utils;

import org.marketplace.user.model.UserProfile;
import org.marketplace.user.model.User;
import org.testcontainers.shaded.org.apache.commons.lang3.RandomStringUtils;

import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;


public class InitUser {

  public static User initRandomUser() {
    return User.builder()
      .id(null)
      .firstName(RandomStringUtils.randomAlphabetic(5))
      .lastName(RandomStringUtils.randomAlphabetic(5))
      .email(RandomStringUtils.randomAlphabetic(5) + "@gmail.com")
      .password((RandomStringUtils.randomAlphabetic(25) + (int) (Math.random() * 190)))
      .birthDate(new Date(ThreadLocalRandom.current().nextInt() * 1000L))
      .city(RandomStringUtils.randomAlphabetic(5))
      .build();
  }

  public static UserProfile initRandomProfile() {
    return UserProfile.builder()
      .id(null)
      .userId((int) (Math.random() * 90))
      .firstName(RandomStringUtils.randomAlphabetic(5))
      .lastName(RandomStringUtils.randomAlphabetic(5))
      .build();
  }

}
